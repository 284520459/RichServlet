/**
 * 
 */
package com.apache.rich.servlet.http.servlet.realize;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.apache.rich.servlet.http.servlet.realize.configuration.RichServletHttpServletConfiguration;

import com.apache.rich.servlet.http.servlet.realize.adapter.ConfigAdapter;
import com.apache.rich.servlet.http.servlet.realize.utils.HttpServletUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ServletContext 实现类
 * @author wanghailing
 *
 */
public class RichServletHttpServletContext extends ConfigAdapter implements
		ServletContext {
	
	 private static final Logger log = LoggerFactory.getLogger(RichServletHttpServletContext.class);
    
	 private Map<String, Object> attributes;

	 private String servletContextName;
	    
	 private static class LazyHolder {
		private static final RichServletHttpServletContext INSTANCE = new RichServletHttpServletContext();
	}

	private RichServletHttpServletContext() {
		super("RichServletHttpServletContext ");
	}

	public static final RichServletHttpServletContext get() {
		return LazyHolder.INSTANCE;
	}

	@Override
    public Object getAttribute(String name) {
        return attributes != null ? attributes.get(name) : null;
    }

    @Override
    public Enumeration getAttributeNames() {
        return HttpServletUtils.enumerationFromKeys(attributes);
    }

    @Override
    public String getContextPath() {
        return "";
    }

    @Override
    public int getMajorVersion() {
        return 2;
    }

    @Override
    public int getMinorVersion() {
        return 4;
    }

    @Override
    public URL getResource(String path) throws MalformedURLException {
        return RichServletHttpServletContext.class.getResource(path);
    }

    @Override
    public InputStream getResourceAsStream(String path) {
        return RichServletHttpServletContext.class.getResourceAsStream(path);
    }

    @Override
    public String getServerInfo() {
        return super.getOwnerName();
    }

    @Override
    public void log(String msg) {
        log.info(msg);
    }

    @Override
    public void log(Exception exception, String msg) {
        log.error(msg, exception);
    }

    @Override
    public void log(String message, Throwable throwable) {
        log.error(message, throwable);
    }

    @Override
    public void removeAttribute(String name) {
        if (this.attributes != null)
            this.attributes.remove(name);
    }

    @Override
    public void setAttribute(String name, Object object) {
        if (this.attributes == null)
            this.attributes = new HashMap<String, Object>();

        this.attributes.put(name, object);
    }

    @Override
    public String getServletContextName() {
        return this.servletContextName;
    }

    void setServletContextName(String servletContextName) {
        this.servletContextName = servletContextName;
    }

    @Override
    public Servlet getServlet(String name) throws ServletException {
        throw new IllegalStateException(
                "Deprecated as of Java Servlet API 2.1, with no direct replacement!");
    }

    @Override
    public Enumeration getServletNames() {
        throw new IllegalStateException(
                "Method 'getServletNames' deprecated as of Java Servlet API 2.0, with no replacement.");
    }

    @Override
    public Enumeration getServlets() {
        throw new IllegalStateException(
                "Method 'getServlets' deprecated as of Java Servlet API 2.0, with no replacement.");
    }

    @Override
    public ServletContext getContext(String uripath) {
        return this;
    }

    @Override
    public String getMimeType(String file) {
        return HttpServletUtils.getMimeType(file);

    }

    @Override
    public Set getResourcePaths(String path) {
        throw new IllegalStateException(
                "Method 'getResourcePaths' not yet implemented!");
    }

    @Override
    public RequestDispatcher getNamedDispatcher(String name) {
        Collection<RichServletHttpServletConfiguration> colls = RichServletHttpServletWebapp.get().getWebappConfig().getServletConfigurations();
        HttpServlet servlet = null;
        for (RichServletHttpServletConfiguration configuration : colls) {
            if (configuration.getConfig().getServletName().equals(name)) {
                servlet = configuration.getHttpComponent();
            }
        }

        return new RichServletHttpRequestDispatcher(name, null, servlet);
    }

    @Override
    public String getRealPath(String path) {
        if ("/".equals(path)) {
            try {
                File file = File.createTempFile("netty-servlet-bridge", "");
                file.mkdirs();
                return file.getAbsolutePath();
            } catch (IOException e) {
                throw new IllegalStateException(
                        "Method 'getRealPath' not yet implemented!");
            }
        } else {
            throw new IllegalStateException(
                    "Method 'getRealPath' not yet implemented!");
        }
    }

    @Override
    public RequestDispatcher getRequestDispatcher(String path) {
        Collection<RichServletHttpServletConfiguration> colls = RichServletHttpServletWebapp.get().getWebappConfig().getServletConfigurations();
        HttpServlet servlet = null;
        String servletName = null;
        for (RichServletHttpServletConfiguration configuration : colls) {
            if (configuration.matchesUrlPattern(path)) {
                servlet = configuration.getHttpComponent();
                servletName = configuration.getHttpComponent().getServletName();
            }
        }

        return new RichServletHttpRequestDispatcher(servletName, path, servlet);
    }

}
