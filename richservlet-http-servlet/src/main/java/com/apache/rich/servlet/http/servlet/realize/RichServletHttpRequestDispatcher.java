package com.apache.rich.servlet.http.servlet.realize;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 
 * @author wanghailing
 *
 */
public class RichServletHttpRequestDispatcher implements RequestDispatcher {

    /**
     * The servlet name for a named dispatcher.
     */
    private String name = null;

    /**
     * The servlet path for this RequestDispatcher.
     */
    private String servletPath = null;


    private HttpServlet httpServlet;


    public RichServletHttpRequestDispatcher(String servletName, String servletPath, HttpServlet servlet) {
        this.name = servletName;
        this.servletPath = servletPath;
        this.httpServlet = servlet;
    }


    @Override
    public void forward(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        if (httpServlet != null) {
            //TODO Wrap
            httpServlet.service(servletRequest, servletResponse);
        } else {
            ((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @Override
    public void include(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        if (httpServlet != null) {
            //TODO Wrap
            httpServlet.service(servletRequest, servletResponse);
        } else {
            ((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
