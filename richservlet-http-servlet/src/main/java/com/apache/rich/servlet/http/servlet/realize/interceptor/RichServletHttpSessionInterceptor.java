
package com.apache.rich.servlet.http.servlet.realize.interceptor;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;

import java.util.Collection;

import com.apache.rich.servlet.http.servlet.realize.session.RichServletHttpSessionStore;

import com.apache.rich.servlet.http.servlet.realize.session.HttpSessionThreadLocal;
import com.apache.rich.servlet.http.servlet.realize.session.RichServletHttpSession;
import com.apache.rich.servlet.http.servlet.realize.utils.HttpServletUtils;

/**
 * 
 * @author wanghailing
 *
 */
public class RichServletHttpSessionInterceptor implements RichServletInterceptor {

    private boolean sessionRequestedByCookie = false;

    public RichServletHttpSessionInterceptor(RichServletHttpSessionStore sessionStore) {
        HttpSessionThreadLocal.setSessionStore(sessionStore);
    }

    @Override
    public void onRequestReceived(ChannelHandlerContext ctx, HttpRequest request) {

        HttpSessionThreadLocal.unset();

        Collection<io.netty.handler.codec.http.cookie.Cookie> cookies = HttpServletUtils.getCookies(
        		RichServletHttpSession.SESSION_ID_KEY, request);
        if (cookies != null) {
            for (io.netty.handler.codec.http.cookie.Cookie cookie : cookies) {
                String jsessionId = cookie.value();
                RichServletHttpSession s = HttpSessionThreadLocal.getSessionStore()
                        .findSession(jsessionId);
                if (s != null) {
                    HttpSessionThreadLocal.set(s);
                    this.sessionRequestedByCookie = true;
                    break;
                }
            }
        }
    }

    @Override
    public void onRequestSuccessed(ChannelHandlerContext ctx, HttpRequest request,
                                   HttpResponse response) {

    		RichServletHttpSession s = HttpSessionThreadLocal.get();
        if (s != null && !this.sessionRequestedByCookie) {
            HttpHeaders.addHeader(response, HttpHeaderNames.SET_COOKIE, ServerCookieEncoder.encode(RichServletHttpSession.SESSION_ID_KEY, s.getId()));
        }

    }

    @Override
    public void onRequestFailed(ChannelHandlerContext ctx, Throwable e,
                                HttpResponse response) {
        this.sessionRequestedByCookie = false;
        HttpSessionThreadLocal.unset();
    }

}
