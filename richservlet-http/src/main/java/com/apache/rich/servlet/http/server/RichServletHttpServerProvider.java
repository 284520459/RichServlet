/**
 * 
 */
package com.apache.rich.servlet.http.server;

import com.apache.rich.servlet.http.server.acceptor.RichServletHttpServerAsyncAcceptor;
/**
 * @author wanghailing
 *
 */
public class RichServletHttpServerProvider{

	private static class LazyHolder {
		private static final RichServletHttpServerProvider INSTANCE = new RichServletHttpServerProvider();
	}

	private RichServletHttpServerProvider() {

	}

	public static final RichServletHttpServerProvider getInstance() {
		return LazyHolder.INSTANCE;
	}

	
	public RichServletHttpServer service() {
			RichServletHttpServer httpServer = new RichServletHttpServer();
            httpServer.ioAcceptor(new RichServletHttpServerAsyncAcceptor(httpServer));
            return httpServer;
	}

}
