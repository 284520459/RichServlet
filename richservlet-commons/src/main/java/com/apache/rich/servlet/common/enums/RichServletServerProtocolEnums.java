package com.apache.rich.servlet.common.enums;

public enum RichServletServerProtocolEnums {
	HTTP, HTTPS, SPDY, HTTP2;
}