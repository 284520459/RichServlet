package com.apache.rich.servlet.core.server.rest.controller;


import java.util.HashMap;
import java.util.Map;

import com.apache.rich.servlet.core.server.rest.HttpURLResource;

import com.apache.rich.servlet.core.server.monitor.RichServletServerMonitor;
import com.apache.rich.servlet.core.annotation.Controller;
import com.apache.rich.servlet.core.annotation.RequestMapping;
import com.apache.rich.servlet.common.enums.RequestMethodEnums;

@Controller
public class DefaultController {

    @RequestMapping(value = "/", method = RequestMethodEnums.GET)
    public RootResponse root() {
        RootResponse response = new RootResponse();
        response.REQUESTS_MISS = RichServletServerMonitor.getRequestMiss();
        response.REQUEST_HITS = RichServletServerMonitor.getRequestHit();
        response.CONNECTIONS = RichServletServerMonitor.getConnections();
        response.LAST_SERV_TIME = RichServletServerMonitor.getLastServTime();
        response.LAST_SERV_ID = RichServletServerMonitor.getLastServID();
        response.LAST_SERV_FAIL_ID = RichServletServerMonitor.getLastServFailID();

        for (Map.Entry<HttpURLResource, URLController> entry : RichServletServerMonitor.getResourcesMap().entrySet())
            response.RESOURCES_HITS.put(entry.getKey().toString(), entry.getValue().count());

        return response;
    }

    public static class RootResponse {
        public long REQUESTS_MISS;
        public long REQUEST_HITS;
        public long CONNECTIONS;
        public long LAST_SERV_TIME = System.currentTimeMillis();
        public String LAST_SERV_ID;
        public String LAST_SERV_FAIL_ID;
        public Map<String, Long> RESOURCES_HITS = new HashMap<>();
    }
}
